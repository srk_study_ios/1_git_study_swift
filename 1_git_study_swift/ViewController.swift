//
//  ViewController.swift
//  1_git_study_swift
//
//  Created by 鶴本 幸大 on 2016/11/30.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let items = ["google", "yahoo", "safari", "youtube", "Google Play", "AppStore", "Picture"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "TableView Sample"
        self.tableView.rowHeight = 100
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "BasicCell")
        
        cell.textLabel?.text = items[indexPath.row]
        return cell
    }
    
    func tableView(_ table: UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath) {
        table.deselectRow(at: indexPath as IndexPath, animated: true)
    }

}

